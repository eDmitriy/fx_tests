﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Example/Diffuse Distance" {
	Properties{
		_MainTex("Texture", 2D) = "white" {}
		_MainTexMult("Texture mult", Color) = (1.0,1.0,1.0,0.3)

		_ShockWaveColor("Shock Wave Color", Color) = (1.0,1.0,1.0,0.3)

		_Center("Center", Vector) = (0,0,0,0)
		_Radius("Radius", Float) = 0.5
		_ImpactRadius("Impact Radius", Float) = 0.5

		_Amount("Extrusion Amount", Range(-9.9,9.9)) = 0

		_JitterMagnitude("Jitter Magnitude", Float) = 0.03
		_JitterSpeed("Jitter Speed", Float) = 300


	}

	SubShader{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		
			ZWrite On 
			Lighting Off 
			//Cull Off 
			Fog{ Mode Off } Blend One Zero

		CGPROGRAM
		#pragma surface surf Lambert vertex:vert alpha

		struct Input {
			float2 uv_MainTex;
			float3 worldPos;
			float3 viewDir;
		};
		sampler2D _MainTex;

		float3 _Center;
		float _Radius;
		float _ImpactRadius;
		float4 _ShockWaveColor;
		float _JitterMagnitude;
		float _JitterSpeed;
		float _Amount;
		float4 _MainTexMult;

		void vert(inout appdata_full v) {
			half z = sin(_Time[1]* _JitterSpeed);

			float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
			float d = _ImpactRadius - distance(_Center, worldPos);
			//d = clamp(d, 0, _ImpactRadius );
			d = sin(-d)/d;
			//if (d < _ImpactRadius)
				v.vertex.xyz += (v.normal * _Amount)*(-d);

			if( abs(v.texcoord.y - z)< 0.1)
				v.vertex.xyz += (v.normal * (v.texcoord.y - z) );

			v.vertex.xyz += v.normal * z *_JitterMagnitude;

		}
		
		//SURF
		void surf(Input IN, inout SurfaceOutput o) {
			float d = distance(_Center, IN.worldPos);
			float dN = 1 - saturate(d / _Radius);

			half4 c = tex2D(_MainTex, IN.uv_MainTex);
					
			o.Albedo = c.rgb * _MainTexMult.rgb;
			o.Emission = c.rgb * _MainTexMult.rgb;

			//half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			half rim = saturate(dot(normalize(IN.viewDir), o.Normal));
			//o.Alpha = c.a * _MainTexMult.a * rim;

			//if (dN > 0.0 && dN < 1)
			if (_Radius > 0 && dN > -1 && dN <1) {
				o.Albedo = lerp(_ShockWaveColor.rgb, c.rgb,  pow(d/_Radius, 2) * rim );
				o.Emission = lerp(_ShockWaveColor.rgb, c.rgb, pow(d / _Radius, 2) * rim );
			}
			o.Alpha = c.a * _MainTexMult.a * rim ;

		}



	ENDCG
	}
		Fallback "Diffuse/Transparent"
}