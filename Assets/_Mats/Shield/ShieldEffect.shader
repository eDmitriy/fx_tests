﻿Shader "Custom/ShieldEffect" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_Decal("Base (RGB)", 2D) = "white" {}

	}

		SubShader{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 2000
		Cull Off

		CGPROGRAM
#pragma surface surf Lambert alpha
	struct Input {
		float2 uv_MainTex;
		float2 uv_Decal;
	};
	sampler2D _MainTex;
	sampler2D _Decal;
	

	void surf(Input IN, inout SurfaceOutput o) {
		half4 cd = tex2D(_Decal, IN.uv_Decal);
		half4 c = tex2D(_MainTex, IN.uv_MainTex);

		o.Albedo = c.rgb + cd.rgb*cd.a;
		//o.Albedo = Lerp(c.rgb, cd.rgb, cd.a);
		o.Alpha = c.a + cd.a;
		o.Emission = 1-cd.a;

	}

	ENDCG
	}


		Fallback "Transparent/Diffuse"
}