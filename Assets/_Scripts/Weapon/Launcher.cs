﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : MonoBehaviour
{
    public Transform prefab;
    public float launchForce = 100f;
    public float fireRate = 0.1f;
    private float currFirerateTime = 0;

    private Camera mainCamera;
    private Ray ray;
    private RaycastHit hit;

    Transform go;
    Rigidbody goRb;


    // Use this for initialization
    void Start () {
		mainCamera = Camera.main;
	}
	
	// Update is called once per frame
	void Update ()
	{
        if (Input.GetMouseButton(0) && Time.time > currFirerateTime)
	    {
            currFirerateTime = Time.time + (1f / fireRate);


            ray = mainCamera.ScreenPointToRay(Input.mousePosition);
	        if (Physics.Raycast(ray, out hit))
	        {
                go = Instantiate(prefab);
                goRb = go.gameObject.AddComponent<Rigidbody>();
                goRb.isKinematic = false;

                go.position = transform.position;
                goRb.AddForce( (hit.point-transform.position).normalized * launchForce, ForceMode.Impulse);
            }


	    }
	}
}
