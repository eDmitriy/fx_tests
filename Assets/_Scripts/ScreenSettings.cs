﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenSettings : MonoBehaviour
{
    private static Vector2 screenRes = Vector2.zero;
    float resMult = 1;


	// Use this for initialization
	void Start () {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        
        if(screenRes.x<1) screenRes = new Vector2(Screen.width, Screen.height); 

        QualitySettings.SetQualityLevel(5);
    }

    public void ChangeResolution(float val)
    {
        resMult += val;
        resMult = Mathf.Clamp(resMult, 0.2f, 1);

        Screen.SetResolution(
            (int)(screenRes.x * resMult),
            (int)(screenRes.y * resMult),
            true, 30
            );
    }

    public void CloseProgram()
    {
        Application.Quit();
    }


}
