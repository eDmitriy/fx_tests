﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiffuseDistance : MonoBehaviour
{
    #region Variables

    //public Transform target;

    private Renderer rend;
    private Material mat;
    private Texture2D tex;

    private ParticleSystem ps;


    [Header("Radius anim")] public float radius_Max = 5;
    //public float radius_Min = 0;
    public float radius_animTime = 3;
    private float radius_curr = 0;

    [Header("Impact Radius anim")]
    public float impactRadius_Max = 5;


    [Header("Amount anim")] public float amount_Max = 5;
    public float amount_Min = 0;


    private float index;

    #endregion

    

    // Use this for initialization
    void Start ()
    {
        ps = GetComponentInChildren<ParticleSystem>();

	    rend = GetComponent<Renderer>();
	    mat = rend.material;
	    rend.material = mat;

        if (mat == null) Destroy(this);

        mat.SetFloat("_Radius",0);
       // mat.SetFloat("_Amount",Mathf.Sin(index) * (amount_Max - radius_curr) );
    }

    public int lerpSpeed = 10;

    private int iterations = 0;
    public int iterations_Max = 100;
    void Update () {
        if (iterations>0)
        {
            index += Time.deltaTime* radius_animTime ;

            mat.SetFloat(
                "_ImpactRadius",
                Mathf.Sin(index)*(impactRadius_Max) * ((float)iterations/(float)iterations_Max)
                );
            mat.SetFloat(
                "_Amount",
                Mathf.Sin(index) * (amount_Max ) * ((float)iterations / (float)iterations_Max)
                //amount_Max
                );

            mat.SetFloat(
                "_Radius",
                //Mathf.Sin(index) * (radius_Max)
                    /*( ( iterations_Max - ( (float)iterations)/3f )
                        /(float)iterations_Max
                    ) * radius_Max*/
                    radius_Max
                );
/*            Color shockWaveColor = mat.GetColor("_ShockWaveColor");
            shockWaveColor.a = Mathf.Sin(index)*(radius_Max - radius_curr);
            mat.SetColor(
                "_ShockWaveColor",
                shockWaveColor
            );*/

            //radius_curr += /*radius_animTime**/ /*(1-radius_curr)**/ lerpSpeed;
            iterations-= lerpSpeed;

            if (iterations == 0)
            {
                iterations = 0;
                //radius_curr = -1;
                index = 0f;
                //radiusAnimNow = false;
                mat.SetFloat("_Radius",0);
                mat.SetFloat("_Amount",0);
            }
        }
    }


    void OnCollisionEnter( Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            mat.SetVector("_Center", contact.point);
            //adius_curr = 0.05f;
            iterations = 100;
            index = 0f;

            var oRb = contact.otherCollider.attachedRigidbody;
            oRb.AddForce(-contact.normal*oRb.velocity.magnitude,ForceMode.Impulse);

            Destroy(oRb.gameObject, 5);

            ps.Emit(new ParticleSystem.EmitParams()
            {
                position = contact.point,
                velocity = -contact.normal*2
            },
            2
            );

        }
    }

}
