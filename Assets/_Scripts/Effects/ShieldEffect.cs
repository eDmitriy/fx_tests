﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ShieldEffect : MonoBehaviour, IPointerDownHandler {

    public Color defaultColor = Color.white;
    public Color impactColor = Color.black;

    public Texture2D tex;
    public Texture2D impactTexture;

    float EffectTime;
    private MeshRenderer renderer;

    private Camera mainCamera;
    private Ray ray;
    private RaycastHit hit;

    private void Start()
    {
        renderer = GetComponent<MeshRenderer>();
        mainCamera = Camera.main;

        tex = new Texture2D(1024, 1024, TextureFormat.RGBA32, false);
        Color fillColor = Color.clear;
        Color[] fillPixels = new Color[tex.width * tex.height];

        for (int i = 0; i < fillPixels.Length; i++)
        {
            fillPixels[i] = fillColor;
        }

        tex.SetPixels(fillPixels);
        tex.Apply();

        GetComponent<Renderer>().material.SetTexture("_Decal",  tex);
    }

    void Update()
    {
        if (EffectTime > 0)
        {
            if (EffectTime < 450 && EffectTime > 400)
            {
                renderer.material.SetColor("_ShieldColor", defaultColor);
            }

            EffectTime -= Time.deltaTime * 1000;

            renderer.material.SetFloat("_EffectTime", EffectTime);
        }

        if (Input.GetMouseButton(0))
        {
            ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100f))
            {
               // print(this.GetType() + " RaycastHit "+hit.textureCoord);

                //renderer.material.SetVector("_Position", transform.InverseTransformPoint(hit.textureCoord));


                Renderer rend = hit.transform.GetComponent<Renderer>();
                MeshCollider meshCollider = hit.collider as MeshCollider;
                if (rend == null || rend.material == null || meshCollider == null)
                    return;

                tex = rend.material.GetTexture("_Decal") as Texture2D;
                Vector2 pixelUV = hit.textureCoord;
                pixelUV.x *= tex.width;
                pixelUV.y *= tex.height;

                pixelUV.x -= impactTexture.width/2;
                pixelUV.y -= impactTexture.height/2;

                //tex.SetPixels( (int)pixelUV.x, (int)pixelUV.y, impactTexture.width, impactTexture.height, impactTexture.GetPixels() );

                Color c = Color.black;
                for (int i = 0; i < impactTexture.width; i++)
                {
                    for (int j = 0; j < impactTexture.height; j++)
                    {
                        c = impactTexture.GetPixel(i, j);

                        if (c.a > 0)
                        {
                            tex.SetPixel(
                                (int)(pixelUV.x + i),
                                (int)(pixelUV.y + j),
                                c
                                );
                        }
                    }
                }
                

                tex.Apply();
            }
        }
    }


    
    public void OnPointerDown(PointerEventData eventData)
    {
/*        print(this.GetType() + " OnPointerDown ");

        UpdateEffect(eventData.pressPosition);*/
    }


    private void UpdateEffect(Vector3 pos)
    {
        renderer.material.SetColor("_ShieldColor", impactColor);

        renderer.material.SetVector("_Position", transform.InverseTransformPoint(pos));

        EffectTime = 500;
    }

/*
    void OnCollisionEnter( Collision collision)
    {
        foreach (ContactPoint contact in collision.contacts)
        {
            renderer.sharedMaterial.SetVector("_ShieldColor", Vector4(0.7, 1, 1, 0.05));

            renderer.sharedMaterial.SetVector("_Position", transform.InverseTransformPoint(contact.point));

            EffectTime = 500;
        }
    }*/
}
